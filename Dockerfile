FROM golang:1.18-alpine3.14 AS builder

RUN apk update && apk add --no-cache git && apk add gcc libc-dev

WORKDIR /app

COPY go.mod .

COPY go.sum .

RUN go mod download

COPY . .

RUN GOARCH=amd64 GOOS=linux go build -o /go/bin/documentdb-migration main.go

FROM alpine:3.11

RUN apk add --no-cache tzdata ca-certificates libc6-compat

COPY --from=builder /go/bin/documentdb-migration /go/bin/documentdb-migration

ENTRYPOINT ["/go/bin/documentdb-migration"]