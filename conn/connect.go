package conn

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

// this utils mongo for unit test
const (
	//DB_ENABLED       = true
	DB_MONGO_NAME = "axisnet"
	// data master
	//DB_MONGO_URI = "mongodb://PrOduct1ON4pps:Xv30^tacC1GetM*2@axisnet-production-master.cluster-cwyvwtx5aghk.ap-southeast-1.docdb.amazonaws.com:27017/?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false"
	DB_MONGO_URI = "mongodb://PrOduct1ONTRKLo65:B3F3#ZXaI^8zf16E@axisnet-production-transaction.cluster-cwyvwtx5aghk.ap-southeast-1.docdb.amazonaws.com:27017/?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false"

	// logging
	//DB_MONGO_URI = "mongodb://PrOduct1ONTRKLo65:B3F3#ZXaI^8zf16E@axisnet-production-trxlogs.cluster-cwyvwtx5aghk.ap-southeast-1.docdb.amazonaws.com:27017/?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false"

	// db read
	//DB_MONGO_URI_READ = "mongodb://PrOduct1ON4pps:Xv30^tacC1GetM*2@axisnet-production-master.cluster-ro-cwyvwtx5aghk.ap-southeast-1.docdb.amazonaws.com:27017/?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false"
	DB_MONGO_URI_READ = "mongodb://PrOduct1ONTRKLo65:B3F3#ZXaI^8zf16E@axisnet-production-transaction.cluster-ro-cwyvwtx5aghk.ap-southeast-1.docdb.amazonaws.com:27017/?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false"
	// db write
	//DB_MONGO_URI_WRITE = "mongodb://PrOduct1ON4pps:Xv30^tacC1GetM*2@axisnet-production-master.cluster-cwyvwtx5aghk.ap-southeast-1.docdb.amazonaws.com:27017/?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false"
	DB_MONGO_URI_WRITE = "mongodb://PrOduct1ONTRKLo65:B3F3#ZXaI^8zf16E@axisnet-production-transaction.cluster-cwyvwtx5aghk.ap-southeast-1.docdb.amazonaws.com:27017/?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false"
)

var (
	mongoClient      *mongo.Client
	mongoClientRead  *mongo.Client
	mongoClientWrite *mongo.Client
	mongoTrx         *mongo.Client
)

func Mongo(url string) *mongo.Client {
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	context.DeadlineExceeded.Error()

	//tlsConfig, err := getCustomTLSConfig("rds-combined-ca-bundle.pem")
	//if err != nil {
	//	log.Fatalf("Failed getting TLS configuration: %v", err)
	//}

	//client, err := mongo.NewClient(options.Client().ApplyURI(url).SetTLSConfig(tlsConfig))
	client, err := mongo.NewClient(options.Client().ApplyURI(url))
	if err != nil {
		err = fmt.Errorf("error construct new mongo-client %v", err)
		panic(err)
	}

	err = client.Connect(ctx)
	if err != nil {
		err = fmt.Errorf("error conenct the mongo-client %v", err)
		panic(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		err = fmt.Errorf("error ping database %v", err)
		panic(err)
	}

	return client
}

// LoadMongo is function for load database non-transaction
func LoadMongo() {
	mongoClient = Mongo(DB_MONGO_URI)
}

// LoadMongo is function for load database non-transaction
func LoadMongoRead() {
	mongoClientRead = Mongo(DB_MONGO_URI_READ)
}

func LoadMongoWrite() {
	mongoClientWrite = Mongo(DB_MONGO_URI_WRITE)
}

// LoadMongoReader is function for load database reader non-transaction
//func LoadMongoReader() {
//	mongoClient = Mongo(DB_MONGO_URI_READER)
//}

// DB is function for connect into database
func DB() *mongo.Database {
	return mongoClient.Database(DB_MONGO_NAME)
}

// DB is function for connect into database
func DBRead() *mongo.Database {
	return mongoClientRead.Database(DB_MONGO_NAME)
}

func DBWrite() *mongo.Database {
	return mongoClientWrite.Database(DB_MONGO_NAME)
}
