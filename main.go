package main

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"mongodb-to-docdb/conn"
	"time"
)

// UserLastLocation ...
type UserLastLocation struct {
	UserCode  string      `bson:"user_code" json:"user_code" `
	Locations []Locations `bson:"locations" json:"locations" `
}

// Locations ...
type Locations struct {
	CGI         string `bson:"cgi" json:"cgi"`
	POC         string `bson:"poc" json:"POC"`
	KabupatenId string `bson:"kabupatenId"`
	ClusterId   string `bson:"clusterId"`
	Time        string `bson:"time"`
}

func main() {

	conn.LoadMongo()
	mongo := conn.DB()
	conn.LoadMongoRead()
	mongoRead := conn.DBRead()
	conn.LoadMongoWrite()
	mongoWrite := conn.DBWrite()

	ctx := context.Background()
	collectionName := "testingDatabase"

	requestCreate := []UserLastLocation{{
		//data 1
		UserCode: "JO4j4WhdsquogRpxabT2Sg==",
		Locations: []Locations{{
			CGI:         "510.11.22191.10062",
			POC:         "AC1",
			KabupatenId: "1173",
			ClusterId:   "NC53",
			Time:        "2021-12-08 13:18:03",
		},
			{
				CGI:         "510.11.22191.10072",
				POC:         "DDC1",
				KabupatenId: "2173",
				ClusterId:   "OC53",
				Time:        "2021-12-08 13:18:03",
			}},
	},
		//data 2
		{
			UserCode: "JO4j4WhdsquogRpxabT3Sg==",
			Locations: []Locations{{
				CGI:         "510.11.22191.10062",
				POC:         "AC1",
				KabupatenId: "1173",
				ClusterId:   "NC53",
				Time:        "2021-12-08 13:18:03",
			}},
		},
	}
	log.Println("====================================================================================")
	log.Println("start create collection name ", collectionName)
	log.Println("====================================================================================")

	err := mongo.CreateCollection(context.TODO(), collectionName)
	if err != nil {
		log.Println("error create collection: ", err)
	}

	log.Println("end create collection")
	log.Println()
	log.Println("====================================================================================")
	log.Println("start insert mongoDB")
	log.Println("====================================================================================")
	for i, v := range requestCreate {

		resp, err := mongoWrite.Collection(collectionName).InsertOne(ctx, v)
		if err != nil {
			log.Println("error insert mongoDB: ", err)
		}
		log.Println("insert data ke-", i)
		log.Println("insert mongoDB with response: ", resp)
	}

	log.Println("end insert mongoDB")
	log.Println()
	log.Println("====================================================================================")
	log.Println("start update mongoDB")
	log.Println("====================================================================================")

	filter := bson.M{"user_code": "JO4j4WhdsquogRpxabT3Sg=="}
	updateLocation := Locations{
		CGI:         "510.11.22191.10062",
		POC:         "AB1",
		KabupatenId: "1273",
		ClusterId:   "ND53",
		Time:        "2021-12-08 13:18:03",
	}
	update := bson.M{"$set": bson.M{"locations": updateLocation}}

	resp, err := mongoWrite.Collection(collectionName).UpdateOne(ctx, filter, update)
	if err != nil {
		log.Println("error updated mongoDB: ", err)
	}
	log.Println("update mongoDB with response: ", resp)

	log.Println("end update mongoDB")
	log.Println()
	log.Println("====================================================================================")
	log.Println("start find one mongoDB")
	log.Println("====================================================================================")
	find := UserLastLocation{}
	filter = bson.M{"user_code": "JO4j4WhdsquogRpxabT2Sg=="}
	err = mongoRead.Collection(collectionName).FindOne(ctx, filter).Decode(&find)
	if err != nil {
		log.Println("error find one mongoDB: ", err)
	}
	log.Println("data find one: ", find)

	log.Println("end find one mongoDB")
	log.Println()
	log.Println("====================================================================================")
	log.Println("start delete one mongoDB")
	log.Println("====================================================================================")
	// delete one
	filter = bson.M{"user_code": "JO4j4WhdsquogRpxabT3Sg=="}

	respDel, err := mongoWrite.Collection(collectionName).DeleteOne(ctx, filter)
	if err != nil {
		log.Println("error delete one mongoDB: ", err)
	}
	log.Println("delete mongoDB with response: ", respDel)

	log.Println("end delete one mongoDB")

	time.Sleep(30 * time.Minute)
}
