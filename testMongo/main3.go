package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	// Timeout operations after N seconds
	connectTimeout  = 5
	queryTimeout    = 30
	username        = "PrOduct1ON4pps"
	password        = "Xv30^tacC1GetM*2"
	clusterEndpoint = "axisnet-production-apps.cluster-cwyvwtx5aghk.ap-southeast-1.docdb.amazonaws.com:27017"

	// Which instances to read from
	readPreference = "secondaryPreferred"
	//mongodb://PrOduct1ON-4pps:AJU5dBMRhG!x7v3b@axisnet-production-apps.cluster-cwyvwtx5aghk.ap-southeast-1.docdb.amazonaws.com:27017/axisnet?replicaSet=rs0&readpreference=secondaryPreferred&retryWrites=true&w=majority&ssl=true&tlsAllowInvalidCertificates=true
	connectionStringTemplate = "mongodb://%s:%s@%s/?replicaSet=rs0&readpreference=%s&retryWrites=true&w=majority&ssl=false&tlsAllowInvalidCertificates=true"
)

func main() {

	connectionURI := fmt.Sprintf(connectionStringTemplate, username, password, clusterEndpoint, readPreference)

	connectionURI = "mongodb+srv://PrOduct1ON-4pps:ZE2y5RVJousPYlIN@axisnet-production-apps.jvfpe.mongodb.net/test?retryWrites=true&w=majority"
	log.Println(connectionURI)

	client, err := mongo.NewClient(options.Client().ApplyURI(connectionURI))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), connectTimeout*time.Second)
	defer cancel()

	err = client.Connect(ctx)
	if err != nil {
		log.Fatalf("Failed to connect to cluster: %v", err)
	}

	// Force a connection to verify our connection string
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatalf("Failed to ping cluster: %v", err)
	}

	fmt.Println("Connected to DocumentDB!")

}
